<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Laravel Dev Test</title>
	<link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>
	<div id="app">
		@yield('content')
	</div>
	
	<script>
	        window.Laravel = <?php echo json_encode([
	            'csrfToken' => csrf_token(),
	        ]); ?>
	</script>
	<script src="{{ mix('js/app.js') }}"></script>
	<script src="{{ mix('js/sweetalert.min.js') }}"></script>
</body>
</html>