import {Errors} from './errors';

export class Form {
	constructor(data) {
		this.originalData = data
		this.errors = new Errors()

		for (let field in data) {
			this[field] = data[field]
		}
	}

	data() {
        let data = {};

        for (let property in this.originalData) {
            data[property] = this[property];
        }

        return data;
	}

	reset() {
		for (let field in originalData) {
			this[field] = ''
		}
	}

	submit(requestType, url) {
            axios[requestType](url, this.data())
                .then(response => {
                	swal({
                		title: 'Success!',
                		text: response.data.message,
                		type: 'success',
                		showCancelButton: false,
                		closeOnConfirm: true
                	},
                	function() {
                		window.location.replace('/products/create')
                	})
                    this.onSuccess(response.data);
                })
                .catch(error => {
                    this.onFail(error.response.data);
                })
	}

	onSuccess(data) {
		this.errors.clear()
		this.reset()
	}

	onFail(errors) {
		this.errors.record(errors)
	}
}